﻿using UnityEngine;
using System.Collections;


//Rotation speed
public class EnemyRotationMovement : MonoBehaviour
{
    [SerializeField]
    private float maximumSpinSpeed = 200;

   //Random rotation speed
    void Start()
    {
        GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-maximumSpinSpeed, maximumSpinSpeed);
    }
}
