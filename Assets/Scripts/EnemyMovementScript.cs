﻿using UnityEngine;
using System.Collections;


//Enemy movement Script
public class EnemyMovementScript : MonoBehaviour
{
    //Setting velocity
    private float initialVelocity = 2f;

    private Rigidbody2D ourRigidbody;

   //Code for controll of movement of the enemy
    void Start()
    {
        ourRigidbody = GetComponent<Rigidbody2D>();

        ourRigidbody.velocity = Vector2.down * initialVelocity;
    }
}
