﻿using UnityEngine;
using System.Collections;


//Bullet move forward script
public class BulletMoveForward : MonoBehaviour 
{

    private float initialVelocity = 5f;

    private Rigidbody2D ourRigidbody;

    // Setting the velocity for the bullet
    void Start()
    {
        ourRigidbody = GetComponent<Rigidbody2D>();

        ourRigidbody.velocity = Vector2.up * initialVelocity;
    }   
}
