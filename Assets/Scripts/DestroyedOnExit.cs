﻿using UnityEngine;
using System.Collections;

// Destroy on leaving view port
public class DestroyedOnExit : MonoBehaviour
{
    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
