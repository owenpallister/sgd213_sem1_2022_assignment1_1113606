﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//Destroy on collision with other object
public class DestroyedOnCollision : MonoBehaviour
{  
    void OnTriggerEnter2D(Collider2D other)
    {        
            Destroy(gameObject);
    }
}
